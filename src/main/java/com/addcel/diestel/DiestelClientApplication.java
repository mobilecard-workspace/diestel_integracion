/**
 * @author Victor Ramirez
 */

package com.addcel.diestel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class DiestelClientApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DiestelClientApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(DiestelClientApplication.class, args);
	}
	
}
