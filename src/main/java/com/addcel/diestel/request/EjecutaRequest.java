/**
 * @author Victor Ramirez
 */

package com.addcel.diestel.request;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EjecutaRequest {

	@NotNull
	private Double cantidad;
	
}
