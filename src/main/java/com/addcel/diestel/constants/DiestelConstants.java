/**
 * @author Victor Ramirez
 */

package com.addcel.diestel.constants;

public class DiestelConstants {
	
	public static final String NOMBRE_CAMPO_GRUPO = "IDGRUPO";
	public static final String NOMBRE_CAMPO_CADENA = "IDCADENA";
	public static final String NOMBRE_CAMPO_TIENDA = "IDTIENDA";
	public static final String NOMBRE_CAMPO_TERMINAL = "IDPOS";
	public static final String NOMBRE_CAMPO_CAJERO = "IDCAJERO";
	public static final String NOMBRE_CAMPO_FECHA = "FECHALOCAL";
	public static final String NOMBRE_CAMPO_HORA = "HORALOCAL";
	public static final String NOMBRE_CAMPO_TRANSACCION = "TRANSACCION";
	public static final String NOMBRE_CAMPO_SKU = "SKU";
	public static final String NOMBRE_CAMPO_TIPOPAGO = "TIPOPAGO";
	public static final String NOMBRE_CAMPO_REFERENCIA = "REFERENCIA";
	
	public static final int LONGITUD_CAMPO_GRUPO = 4;
	public static final int LONGITUD_CAMPO_CADENA = 5;
	public static final int LONGITUD_CAMPO_TIENDA = 5;
	public static final int LONGITUD_CAMPO_TERMINAL = 4;
	public static final int LONGITUD_CAMPO_CAJERO = 10;
	public static final int LONGITUD_CAMPO_FECHA = 8;
	public static final int LONGITUD_CAMPO_HORA = 8;
	public static final int LONGITUD_CAMPO_TRANSACCION = 12;
	public static final int LONGITUD_CAMPO_SKU = 13;
	public static final int LONGITUD_CAMPO_REFERENCIA = 120;
	
	public static final int CLASS_READ_SHOW_NORETURN = 0;
	public static final int CLASS_READ_SHOW_RETURN = 1;
	public static final int CLASS_NOREAD_NOSHOW_DEF_RETURN = 2;
	public static final int CLASS_NOREAD_NOSHOW_SYS_RETURN = 3;
	public static final int CLASS_NOREAD_CONFIRM_RETURN = 4;
	public static final int CLASS_NOREAD_NOCONFIRM_RETURN = 5;
	public static final int CLASS_PRINT_LABEL = 6;
	public static final int CLASS_PRINT_NOLABEL = 7;
	public static final int CLASS_READ_COMBO_RETURN = 8;
	
	public static final String FORMAT_DATE = "dd/MM/yyyy";
	public static final String FORMAT_HOUR = "HH:mm:ss";
	
}
