/**
 * @author Victor Ramirez
 */

package com.addcel.diestel.util;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.diestel.constants.DiestelConstants;

import diestel.wsdl.CCampo;
import diestel.wsdl.ETipo;

@Component
public class DiestelUtil {
	
	@Autowired
	private PropertiesFile propsFile;
	
	private static final Logger LOGGER = LogManager.getLogger(DiestelUtil.class);

	public void insertaCampos(List<CCampo> campos, int idApp) {
		CCampo grupo = new CCampo();
		grupo.setSCampo(DiestelConstants.NOMBRE_CAMPO_GRUPO);
		grupo.setSValor(propsFile.getDiestelIdgrupo());
		grupo.setILongitud(DiestelConstants.LONGITUD_CAMPO_GRUPO);
		grupo.setITipo(ETipo.NE);
		grupo.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		grupo.setBEncriptado(false);
		
		CCampo cadena = new CCampo();
		cadena.setSCampo(DiestelConstants.NOMBRE_CAMPO_CADENA);
		cadena.setSValor(propsFile.getDiestelIdcadena());
		cadena.setILongitud(DiestelConstants.LONGITUD_CAMPO_CADENA);
		cadena.setITipo(ETipo.NE);
		cadena.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		cadena.setBEncriptado(false);
		
		CCampo tienda = new CCampo();
		tienda.setSCampo(DiestelConstants.NOMBRE_CAMPO_TIENDA);
		tienda.setSValor(propsFile.getDiestelIdtienda());
		tienda.setILongitud(DiestelConstants.LONGITUD_CAMPO_TIENDA);
		tienda.setITipo(ETipo.NE);
		tienda.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		tienda.setBEncriptado(false);
		
		CCampo terminal = new CCampo();
		terminal.setSCampo(DiestelConstants.NOMBRE_CAMPO_TERMINAL);
		terminal.setSValor(idApp);
		terminal.setILongitud(DiestelConstants.LONGITUD_CAMPO_TERMINAL);
		terminal.setITipo(ETipo.NE);
		terminal.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		terminal.setBEncriptado(false);
		
		CCampo cajero = new CCampo();
		cajero.setSCampo(DiestelConstants.NOMBRE_CAMPO_CAJERO);
		cajero.setSValor(idApp);
		cajero.setILongitud(DiestelConstants.LONGITUD_CAMPO_CAJERO);
		cajero.setITipo(ETipo.NE);
		cajero.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		cajero.setBEncriptado(false);
		
		CCampo fecha = new CCampo();
		fecha.setSCampo(DiestelConstants.NOMBRE_CAMPO_FECHA);
		fecha.setSValor(this.getFecha());
		fecha.setILongitud(DiestelConstants.LONGITUD_CAMPO_FECHA);
		fecha.setITipo(ETipo.AN);
		fecha.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		fecha.setBEncriptado(false);
		
		CCampo hora = new CCampo();
		hora.setSCampo(DiestelConstants.NOMBRE_CAMPO_HORA);
		hora.setSValor(this.getHora());
		hora.setILongitud(DiestelConstants.LONGITUD_CAMPO_HORA);
		hora.setITipo(ETipo.AN);
		hora.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		hora.setBEncriptado(false);
		
		CCampo transaccion = new CCampo();
		transaccion.setSCampo(DiestelConstants.NOMBRE_CAMPO_TRANSACCION);
		transaccion.setSValor();
		transaccion.setILongitud(DiestelConstants.LONGITUD_CAMPO_TRANSACCION);
		transaccion.setITipo(ETipo.NE);
		transaccion.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		transaccion.setBEncriptado(false);
		
		CCampo sku = new CCampo();
		sku.setSCampo(DiestelConstants.NOMBRE_CAMPO_SKU);
		sku.setSValor();
		sku.setILongitud(DiestelConstants.LONGITUD_CAMPO_SKU);
		sku.setITipo(ETipo.AN);
		sku.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		sku.setBEncriptado(false);
		
		CCampo referencia = new CCampo();
		referencia.setSCampo(DiestelConstants.NOMBRE_CAMPO_REFERENCIA);
		referencia.setSValor();
		referencia.setILongitud(DiestelConstants.LONGITUD_CAMPO_REFERENCIA);
		referencia.setITipo(ETipo.AN);
		referencia.setIClase(DiestelConstants.CLASS_READ_SHOW_RETURN);
		referencia.setBEncriptado(false);
		
		campos.add(grupo);
		campos.add(cadena);
		campos.add(tienda);
		campos.add(terminal);
		campos.add(cajero);
		campos.add(fecha);
		campos.add(hora);
		campos.add(transaccion);
		campos.add(sku);
		campos.add(referencia);
		
	}
	
	private String getFecha() {
		String fecha = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat(DiestelConstants.FORMAT_DATE);
		
		return fecha;
	}
	
	private String getHora() {
		String hora = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat(DiestelConstants.FORMAT_HOUR);
		
		return hora;
	}
	
}
