/**
 * @author Victor Ramirez
 */

package com.addcel.diestel.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${diestel.ws.url}")
	private String diestelWsUrl;
	
	@Value("${diestel.ws.info}")
	private String diestelWsInfo;
	
	@Value("${diestel.ws.ejecuta}")
	private String diestelWsEjecuta;
	
	@Value("${diestel.ws.reversa}")
	private String diestelWsReversa;
	
	@Value("${diestel.action.base}")
	private String diestelActionBase;
	
	@Value("${diestel.ws.timeout}")
	private int diestelWsTimeout;
	
	@Value("${diestel.idgrupo}")
	private int diestelIdgrupo;
	
	@Value("${diestel.idcadena}")
	private int diestelIdcadena;
	
	@Value("${diestel.idtienda}")
	private int diestelIdtienda;
	
}
