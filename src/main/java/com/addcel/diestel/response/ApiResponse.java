/**
 * @author Victor Ramirez
 */

package com.addcel.diestel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

	private int code;
	private String message;
    private Object data;
	
}
