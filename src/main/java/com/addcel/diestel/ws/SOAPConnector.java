/**
 * @author Victor Ramirez
 */

package com.addcel.diestel.ws;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;

import com.addcel.diestel.util.PropertiesFile;

public class SOAPConnector extends WebServiceGatewaySupport {
	
	@Autowired
	private PropertiesFile propsFile;

	private static final Logger LOGGER = LogManager.getLogger(SOAPConnector.class);

	/**
	 * Call SOAP WebService with HTTP Headers and SOAPACtion header
	 * @param request the Request body SOAP Envelope
	 * @param soapAction the SOAPAction header
	 * @return
	 */
	public Object callWebService(Object request, String soapAction){
		LOGGER.debug("Realizando la peticion a la URL: " + propsFile.getDiestelWsUrl());
		
        return getWebServiceTemplate().marshalSendAndReceive(propsFile.getDiestelWsUrl(), request,
        		new WebServiceMessageCallback() {
		            public void doWithMessage(WebServiceMessage message) {
		            	((SoapMessage)message).setSoapAction(propsFile.getDiestelActionBase() + soapAction);
		            	
		            	TransportContext context = TransportContextHolder.getTransportContext();
		            	HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
		            	
		            	connection.getConnection().setConnectTimeout(propsFile.getDiestelWsTimeout());
		            	connection.getConnection().setReadTimeout(propsFile.getDiestelWsTimeout());
		            	//connection.getConnection().addRequestProperty("user", propsFile.getPrevivaleWsUsername());
		        	    //connection.getConnection().addRequestProperty("password", propsFile.getPrevivaleWsPassword());
		        	    
		        	    LOGGER.debug("SOAPAction: " + ((SoapMessage)message).getSoapAction());
		        	    //LOGGER.debug("HTTP Header - User: " + connection.getConnection().getRequestProperty("user"));
		        	    //LOGGER.debug("HTTP Header - Password: " + connection.getConnection().getRequestProperty("password").replaceAll("[a-zA-Z0-9]", "*"));
		            }
	            });
    }
	
}
