/**
 * @author Victor Ramirez
 */

package com.addcel.diestel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.addcel.diestel.ws.SOAPConnector;

@Configuration
public class DiestelConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("previvale.wsdl");
		
		return marshaller;
	}

	@Bean
    public SOAPConnector soapConnector(Jaxb2Marshaller marshaller) {
        SOAPConnector client = new SOAPConnector();
        client.setDefaultUri("http://201.148.36.253/wcf/wsprevivale.WSprevi.svc");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        
        return client;
    }
	
}
